import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    initialData:{},
    filterData: [],
    optionsArray: ['Только прямые', 'Только с багажом', 'Только возвратные'],
    optionsFilteredArray:[],
    companiesFilteredArray:[]
  },
  getters:{
    propsData(state){
      return state.filterData
    }
  },
  mutations: {
    //fetch initial data from JSON file
    fetchData(state){
      axios.get('./results.json')
      .then(res => {
        //store initial data
        state.initialData = res.data
        for (var i = 0; i < state.initialData.flights.length; i++){
        let c = state.initialData.flights[i];
          let o = {
            id: i,
            refundable: c.refundable,
            price: c.price + ' ' + c.currency,
            carrier: c.itineraries[0][0].carrier,
            stops: c.itineraries[0][0].stops,
            carrier_name: c.itineraries[0][0].carrier_name,
            baggage: c.itineraries[0][0].segments[0].baggage_options[0].value,
            baggage_unit: c.itineraries[0][0].segments[0].baggage_options[0].unit,
            total_time: c.itineraries[0][0].traveltime,
            dep_time: c.itineraries[0][0].segments[0].dep_time,
            arr_time: c.itineraries[0][0].stops > 0 ? c.itineraries[0][0].segments[1].arr_time : c.itineraries[0][0].segments[0].arr_time,
            origin_code: c.itineraries[0][0].segments[0].origin_code,
            dest_code: c.itineraries[0][0].stops > 0 ? c.itineraries[0][0].segments[1].dest_code : c.itineraries[0][0].segments[0].dest_code,
            stop_location: c.itineraries[0][0].stops > 0 ? c.itineraries[0][0].segments[1].origin : ' ',
            dep_time_iso: c.itineraries[0][0].stops > 0 ? c.itineraries[0][0].segments[1].dep_time_iso : ' ',
            arr_time_iso: c.itineraries[0][0].stops > 0 ? c.itineraries[0][0].segments[0].arr_time_iso : ' '
          }
          state.filterData.push(o);
        }
      })
      .catch(err => console.log(err))
    },
    //clear filtered arrays
    clearFilteredArray(state){
      state.optionsFilteredArray = []
    },
    clearAviaFilteredArray(state){
      state.companiesFilteredArray = []
    },
    toggleDataArray(state, {option, arr}){
      if(arr === 'option'){
        state.optionsFilteredArray.indexOf(option) === -1 ?  state.optionsFilteredArray.push(option) :  state.optionsFilteredArray.splice(state.optionsFilteredArray.indexOf(option), 1);
      } else {
        state.companiesFilteredArray.indexOf(option) === -1 ?  state.companiesFilteredArray.push(option) :  state.companiesFilteredArray.splice(state.companiesFilteredArray.indexOf(option), 1);
      }
    }
  },
  actions: {

  }
})
